export default {
    API_ROOT: "http://localhost:24609/api",
    
    GET_USER: "/users/useruid",

    GET_DOCUMENT_BY_CUSTOMER: "/documents/getbycustomer",
}