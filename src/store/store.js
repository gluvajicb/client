import Vue from 'vue'
import Vuex from 'vuex'
import userModule from '../modules/user'
import documentModule from '../modules/document'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user: userModule,
    document: documentModule
  }
})

export default store