import Vue from 'vue'
import Router from 'vue-router'

import SignIn from './components/Auth/SignIn.vue'
import SignUp from './components/Auth/SignUp.vue'
import CreateDocument from './components/Documents/CreateDocument.vue'
import ViewDocuments from './components/Documents/ViewDocuments.vue'


Vue.use(Router)

export default new Router({
    mode: 'history',
    
    routes: [
        {
            path: '/',
            component: SignIn
        },
        {
            path: '/sign-in',
            component: SignIn
        }, 
        {
            path: '/sign-up',
            component: SignUp
        }, 
        {
            path: '/create-document',
            component: CreateDocument
        }, 
        {
            path: '/view-documents',
            component: ViewDocuments
        },
    ]
})