import Vuex from 'vuex'
import Vue from 'vue'
import documentService from '../services/DocumentService'

Vue.use(Vuex)

export default {
  state: {
    currentDocument: {},
    documents: []
  },
  
  getters: {
      getDocuments: state => state.documents,

      getCurrentDocument: state => state.currentDocument
  },
  
  mutations: {
      SET_DOCUMENTS(state, payload) {
          state.documents = payload
      },

      SET_CURRENT_DOCUMENT(state, payload) {
          state.currentDocument = payload
      }
  },
  
  actions: {
      async setDocuments(context, customerUid) {
        const payload = await documentService.getAllByCustomer(customerUid);

        context.commit('SET_DOCUMENTS', payload);
      },

      async createDocument(context, document) {
          const payload = await documentService.createDocument(document);

          context.commit('SET_CURRENT_DOCUMENT', payload)
      }
  }
};