import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default {
  state: {
    user: {}
  },
  
  getters: {
      getUser: state => state.user,
  },
  
  mutations: {
      SET_USER(state, payload) {
          state.user = payload
      },
  },
  
  actions: {
      async setUser(context, user) {
        context.commit('SET_USER', user);
      },
  }
};