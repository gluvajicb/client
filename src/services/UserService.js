import config from '../config';
import AXIOS from 'axios'

class UserService {
    async getUser(userUid) {
        const response = await AXIOS.get(config.API_ROOT + config.GET_USER + '?useruid=' + userUid);

        return response.data;
    }
}

export default new UserService();