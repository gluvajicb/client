import config from '../config';
import AXIOS from 'axios'

class DocumentService {
    async getAllByCustomer(customerUid) {
        const response = await AXIOS.get(config.API_ROOT + config.GET_DOCUMENT_BY_CUSTOMER + '?customerUid=' + customerUid);

        return response.data;
    }
}

export default new DocumentService();