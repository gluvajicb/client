import axios from 'axios';

const API_URL = 'http://localhost:5088/api';

const authService = {
    async signUp(request) {
        return await axios.post(API_URL + "/auth/register", request);
    }
};

export default authService;